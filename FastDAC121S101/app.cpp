#include "APP.h"
#include "sin.h"

size_t position = 0;

void TIM1_UP_IRQHandler()
{
	// Pull CS Pin low
	HAL_GPIO_WritePin(SPI2_CS_GPIO_Port, SPI2_CS_Pin, GPIO_PIN_RESET);
	// Write data to spi register
	// This will automatically initialize transmission
	hspi2.Instance->DR = SIN_TABLE_5[position];
	// Change position in sin table
	position = (position + 1) % 5;

	// Clear timer interrupt flag
	// Forgetting this instruction will cause the cpu to keep calling this ISR
	__HAL_TIM_CLEAR_IT(&htim1, TIM_IT_UPDATE);
}

void SPI2_IRQHandler()
{	
	// Get source of the interrupt
	// Not required
	volatile uint32_t itsource = hspi2.Instance->CR2;

	// Wait for transmission to complete
	while (!__HAL_SPI_GET_FLAG(&hspi2, SPI_FLAG_RXNE))
	{
	}

	// Read SPI data register to tell pull one item from the FIFO
	// Withtout this instruction the fifo will overflow
	volatile uint16_t dummy = hspi2.Instance->DR;

	// Pull reset pin high
	HAL_GPIO_WritePin(SPI2_CS_GPIO_Port, SPI2_CS_Pin, GPIO_PIN_SET);

	//__HAL_SPI_DISABLE_IT(&hspi2, (SPI_IT_RXNE));
}

void App()
{
	// Enable SPI interface and Receive interrupt
	__HAL_SPI_ENABLE_IT(&hspi2, (SPI_IT_RXNE));
	__HAL_SPI_ENABLE(&hspi2);

	// Start timer
	HAL_TIM_Base_Start_IT(&htim1);

	while(true)
	{
		
	}
}