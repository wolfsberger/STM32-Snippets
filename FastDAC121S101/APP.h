#pragma once

#include "stm32f1xx_hal.h"
#include "stm32f1xx_hal_spi.h"

extern "C" {
	// Timer 1 Update interrupt
	void TIM1_UP_IRQHandler();

	// SPI TX interrupt
	void SPI2_IRQHandler();

	// Application entry point
	void App();
}

extern SPI_HandleTypeDef hspi2;
extern TIM_HandleTypeDef htim1;